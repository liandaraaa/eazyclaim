package ${escapeKotlinIdentifiers(packageName)}

import android.os.Bundle
import ${getMaterialComponentName('android${SupportPackage}.app.Fragment', useAndroidX)}
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nbs.nucleosnucleo.presentation.BaseFragment
<#if !includeLayout>import android.widget.TextView</#if>
<#if applicationPackage??>
import ${applicationPackage}.R
</#if>

class ${className} : BaseFragment() {
     companion object {
        @JvmStatic
        fun newInstance(pageTitle: PageTitle): ${className} {
            val fragment = ${className}()
            val bundle = Bundle()

            fragment.arguments = bundle

            return fragment
        }
    }

    override val layoutResource = R.layout.${fragmentName}

    override fun initIntent() {
    }

    override fun initLib() {
    }

    override fun initUI() {
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

}
