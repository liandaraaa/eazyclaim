<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>
    <@kt.addAllKotlinDependencies />
    <#if needToCache>
    <instantiate from="root/src/app_package/BlankDao.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/data/${packName}/local/${className}Dao.kt" />

    <instantiate from="root/src/app_package/BlankEntity.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/data/${packName}/local/${className}Entity.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/data/${packName}/local/${className}Dao.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/data/${packName}/local/${className}Entity.kt" />
    </#if>
    <instantiate from="root/src/app_package/BlankRequest.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/data/${packName}/model/request/${className}Request.kt" />

    <instantiate from="root/src/app_package/BlankItem.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/data/${packName}/model/response/${className}Item.kt" />

    <instantiate from="root/src/app_package/BlankApi.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/data/${packName}/remote/${className}Api.kt" />

    <instantiate from="root/src/app_package/BlankApiClient.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/data/${packName}/remote/${className}ApiClient.kt" />

    <instantiate from="root/src/app_package/BlankDataStore.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/data/${packName}/${className}DataStore.kt" />

    <instantiate from="root/src/app_package/BlankRepository.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/data/${packName}/${className}Repository.kt" />

   
    <open file="${escapeXmlAttribute(srcOut)}/data/${packName}/model/response/${className}Item.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/data/${packName}/remote/${className}Api.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/data/${packName}/remote/${className}ApiClient.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/data/${packName}/${className}DataStore.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/data/${packName}/${className}Repository.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/data/${packName}/remote/${className}ApiClient.kt" />



    <instantiate from="root/src/app_package/BlankInteractor.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/domain/${packName}/${className}Interactor.kt" />

    <instantiate from="root/src/app_package/BlankUseCase.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/domain/${packName}/${className}UseCase.kt" />

    <instantiate from="root/src/app_package/BlankMapper.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/domain/${packName}/model/${className}Mapper.kt" />

    <instantiate from="root/src/app_package/BlankData.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/domain/${packName}/model/${className}.kt" />  

    <open file="${escapeXmlAttribute(srcOut)}/domain/${packName}/${className}Interactor.kt" /> 
    <open file="${escapeXmlAttribute(srcOut)}/domain/${packName}/${className}UseCase.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/domain/${packName}/model/${className}Mapper.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/domain/${packName}/model/${className}.kt" />                 
</recipe>
