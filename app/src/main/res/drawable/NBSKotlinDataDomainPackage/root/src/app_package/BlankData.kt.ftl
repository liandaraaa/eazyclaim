package ${escapeKotlinIdentifiers(packageName)}.domain.${packName}.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ${className}(
        val id: Int
) : Parcelable