package ${escapeKotlinIdentifiers(packageName)}.domain.${packName}

import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.request.${className}Request
import ${escapeKotlinIdentifiers(packageName)}.domain.${packName}.model.${className}
import io.reactivex.Single

interface ${className}UseCase {
    fun ${callMethod}(<#if apiCallRequest>${className?uncap_first}Request: ${className}Request</#if>): Single<${className}>
}