package ${escapeKotlinIdentifiers(packageName)}.data.${packName}.remote

import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.request.${className}Request
import com.nbs.nucleo.data.WebApi

class ${className}Api(private val apiClient: ${className}ApiClient) : WebApi, ${className}ApiClient {
    override fun ${callMethod}(<#if apiCallRequest>${className?uncap_first}Request: ${className}Request</#if>) = apiClient.${callMethod}(<#if apiCallRequest>${className?uncap_first}Request</#if>)
}