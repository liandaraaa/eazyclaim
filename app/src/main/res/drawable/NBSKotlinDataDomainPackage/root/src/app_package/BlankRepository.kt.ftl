package ${escapeKotlinIdentifiers(packageName)}.data.${packName}

import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.request.${className}Request
import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.response.${className}Item
import com.nbs.nucleo.data.BaseRepository
import io.reactivex.Single
import retrofit2.http.Body

interface ${className}Repository : BaseRepository {
    fun ${callMethod}(<#if apiCallRequest>@Body ${className?uncap_first}Request: ${className}Request</#if>): Single<${className}Item>
}