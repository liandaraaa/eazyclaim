package ${escapeKotlinIdentifiers(packageName)}.domain.${packName}

import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.${className}Repository
import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.request.${className}Request
import ${escapeKotlinIdentifiers(packageName)}.domain.${packName}.model.${className}
import ${escapeKotlinIdentifiers(packageName)}.domain.${packName}.model.${className}Mapper
import io.reactivex.Single

class ${className}Interactor(private val repository: ${className}Repository) : ${className}UseCase {
    override fun ${callMethod}(<#if apiCallRequest>${className?uncap_first}Request: ${className}Request</#if>): Single<${className}> {
        return repository.${callMethod}(<#if apiCallRequest>${className?uncap_first}Request</#if>).map { ${className}Mapper.map${className}(it) }
    }
}