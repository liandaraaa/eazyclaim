package ${escapeKotlinIdentifiers(packageName)}.data.${packName}.local

import android.arch.persistence.room.Entity
import ${escapeKotlinIdentifiers(packageName)}.model.response.${className}Item
import com.google.gson.annotations.SerializedName
import com.nbs.nucleosnucleo.Model

@Entity(tableName = "${className?lower_case}")
data class ${className}Entity(

        @PrimaryKey
        val id: Int

) : Model()

object ${className}EntityMapper {
    val mapItemToEntity: (${className}Item) -> ${className}Entity = { item ->
        with(item) {
            ${className}Entity(
                id = id
            )
        }
    }

    val mapEntityToItem: (${className}Entity) -> ${className}Item = { item ->
        with(item) {
            ${className}Item(
                id = id
            )
        }
    }
}
