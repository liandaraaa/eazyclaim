package ${escapeKotlinIdentifiers(packageName)}.domain.${packName}.model

import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.response.${className}Item

object ${className}Mapper {
    val map${className}: (${className}Item) -> ${className} = { item ->
        with(item) {
            ${className}(
                id = id ?: 0
            )
        }
    }
}