package ${escapeKotlinIdentifiers(packageName)}.data.${packName}.remote

import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.request.${className}Request
import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.response.${className}Item
import com.nbs.nucleo.data.model.ApiResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET

interface ${className}ApiClient {

    @GET("5bfcfb53310000290039bfeb")
    fun ${callMethod}(<#if apiCallRequest>@Body ${className?uncap_first}Request: ${className}Request</#if>): Single<Response<ApiResponse<${className}Item>>>

}
