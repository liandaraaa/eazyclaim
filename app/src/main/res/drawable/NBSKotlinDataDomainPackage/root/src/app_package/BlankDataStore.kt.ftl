package ${escapeKotlinIdentifiers(packageName)}.data.${packName}

import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.request.${className}Request
import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.response.${className}Item
import ${escapeKotlinIdentifiers(packageName)}.data.${packName}.remote.${className}Api
import com.nbs.nucleo.utils.rx.operators.singleApiError
import io.reactivex.Single

class ${className}DataStore(
    <#if needToCache>
       api: ${className}Api,
       db: ${className}Dao?)
    <#else>
       api: ${className}Api)
    </#if>
    : ${className}Repository {
    override val webService = api
    <#if needToCache>
    override val dbService = db
    <#else>
    override val dbService = null
    </#if>
    override fun ${callMethod}(<#if apiCallRequest>${className?uncap_first}Request: ${className}Request</#if>): Single<${className}Item> {
    <#if needToCache>
        return if (dbService?.isCached()?.blockingGet() == false) {
            webService.${callMethod}(<#if apiCallRequest>${className?uncap_first}Request</#if>)
                    .lift(singleApiError())
                    .map { it.data }
                    .doOnSuccess {
                        dbService.save( ${className}EntityMapper.mapItemToEntity(it!!) )
                        debug { "Saving to DB" }
                    }.map { it }
        } else {
            debug { "Loading from DB" }
            return dbService?.getList()?.map { ${className}EntityMapper.mapEntityToItem(it[0]) }!!
        }
    <#else>
        return webService.${callMethod}(<#if apiCallRequest>${className?uncap_first}Request</#if>)
                    .lift(singleApiError())
                    .map { it.data }
    </#if>
    }



}
