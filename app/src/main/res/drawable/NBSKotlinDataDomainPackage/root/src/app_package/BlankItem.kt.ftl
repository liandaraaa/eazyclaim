package ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.response

import android.os.Parcelable
import com.nbs.nucleosnucleo.Model
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ${className}Item(

        val id: Int?

) : Parcelable, Model()
