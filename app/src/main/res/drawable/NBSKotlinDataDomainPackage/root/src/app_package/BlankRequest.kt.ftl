package ${escapeKotlinIdentifiers(packageName)}.data.${packName}.model.request

import com.nbs.nucleosnucleo.RequestModel

data class ${className}Request(val id: Int = 0) : RequestModel()
