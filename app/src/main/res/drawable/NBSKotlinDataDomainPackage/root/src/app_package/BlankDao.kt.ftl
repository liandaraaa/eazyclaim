package ${escapeKotlinIdentifiers(packageName)}.data.${packName}.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.nbs.nucleo.data.RxLocalDb
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

@Dao
abstract class ${className}Dao : RxLocalDb<${className}Entity> {

    @Query("SELECT * FROM ${className?lower_case} WHERE id = :intId")
    abstract override fun get(intId: Int?): Single<${className}Entity>

    @Query("SELECT * FROM ${className?lower_case}")
    abstract override fun getList(): Single<List<${className}Entity>>

    @Query("DELETE FROM ${className?lower_case} WHERE id = :intId")
    abstract override fun remove(intId: Int?)

    @Query("DELETE FROM ${className?lower_case}")
    abstract override fun removeAll()

    override fun isCached(): Single<Boolean> {
        return getList().subscribeOn(Schedulers.io()).map { !it.isEmpty() }
    }

    override fun isCacheExpired(): Single<Boolean> {
        return Single.just(false)
    }

    override fun isItemCached(intId: Int?, strId: String?): Single<Boolean> {
        return Single.just(false)
    }

    override fun isItemCacheExpired(intId: Int?, strId: String?): Single<Boolean> {
        return Single.just(true)
    }
}
