package ${escapeKotlinIdentifiers(packageName)}

import android.content.Context
import com.nbs.nucleosnucleo.presentation.BaseActivity
import org.jetbrains.anko.startActivity
<#if (includeCppSupport!false) && generateLayout>
import kotlinx.android.synthetic.main.${layoutName}.*
</#if>

class ${activityClass} : BaseActivity() {

companion object {
        fun start(context: Context) {
            context.startActivity<${activityClass}>()
        }
    }
    
    override val layoutResource = R.layout.${layoutName}

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

}
