package com.eazyclaim.android.util

import android.content.Context
import android.net.Uri
import android.os.Environment
import android.os.StatFs

/**
 * Created by Dimas Prakoso on 29/05/2019.
 */

fun getSaveDir(title: String): String {
    return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + "/nussa/$title"
}

fun getNameFromUrl(url: String): String {
    return Uri.parse(url).lastPathSegment
}

fun createFilePath(title: String, url: String): String {
    val pathFile = "${getSaveDir(title)}/${getNameFromUrl(url)}"
    return pathFile
}

fun getInternalStoragePath(context: Context, folderName: String, title: String? = null): String {
    val path = if (title != null) "${context.filesDir.absolutePath}/$folderName/$title"
            else  "${context.filesDir.absolutePath}/$folderName"
    return path
}

fun getPrivateExternalStoragePath(context: Context, folderName: String, title: String? = null): String {
    val path = if (title != null) "${context.getExternalFilesDir(folderName)}/$title"
            else "${context.getExternalFilesDir(folderName)}"
    return path
}

fun isExternalMemorySizeAvailable(): Boolean {
    return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
}

fun isInternalMemorySizeAvailable(): Boolean {
    val path = Environment.getDataDirectory()
    val stat = StatFs(path.path)
    val blockSize = stat.blockSize.toLong()
    val availableBlock = stat.availableBlocks.toLong()
    val available = availableBlock * blockSize
    val isAvailable = available > 10240
    return isAvailable
}



