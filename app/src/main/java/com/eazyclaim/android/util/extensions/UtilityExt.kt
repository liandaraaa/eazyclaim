package com.eazyclaim.android.util.extensions

import android.content.res.Resources
import android.view.WindowManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nbs.nucleo.utils.debug
import com.nbs.nucleo.utils.extensions.addTo
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import com.nbs.nucleo.utils.error as debugError

fun RxPermissions.requestPermission(
    vararg permissions: String,
    disposable: CompositeDisposable,
    onSuccess: (isGranted: Boolean) -> Unit
) {
    this.request(*permissions)
        .subscribe(
            {
                onSuccess.invoke(it)
                debug { "do we have permissions granted? $it" }
            }, {
                debugError { "Error requesting permissions!! ${it.printStackTrace()}" }
            }).addTo(disposable)
}

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun Any.toHashMap(): Map<String, String> {
    val gson = Gson()
    val json = gson.toJson(this, this.javaClass)
    val map: Map<String, String>
    val type = object : TypeToken<Map<String, String>>() {}.type
    map = gson.fromJson(json, type)
    return map
}