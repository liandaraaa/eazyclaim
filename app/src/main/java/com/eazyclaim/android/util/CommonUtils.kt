package com.eazyclaim.android.util

import android.annotation.SuppressLint
import android.text.format.DateUtils
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.DayOfWeek
import java.util.Calendar
import java.util.Currency
import java.util.Date
import java.util.Locale
import java.util.TimeZone
import kotlin.math.roundToInt

fun getCurrency(price: Double): String {
    val indonesia = Locale("id", "ID")
    val swedishFormat = NumberFormat.getCurrencyInstance(indonesia)
    return swedishFormat.format(price)
}

fun getValueCurrency(price: Double): String {
    val indonesia = Locale("id", "ID")
    val swedishFormat = NumberFormat.getCurrencyInstance(indonesia)
    return getNormalizedValue(swedishFormat.format(price))
}

fun getNormalizedValue(value: String): String {
    return value.substring(2, value.length)
}

fun getReadableHumanDate(date: String): String {
    val months = arrayOf(
        "Januari",
        "Februari",
        "Maret",
        "April",
        "Mei",
        "Juni",
        "Juli",
        "Agustus",
        "September",
        "Oktober",
        "November",
        "Desember"
    )
    //2018-06-23 09:16:00
    val d = date.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val m = d[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

    return m[2] + " " + months[Integer.parseInt(m[1]) - 1] + " " + m[0]
}

fun getReadableHumanDateWithTime(date: String): String {
    val months =
        arrayOf("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des")
    //2017-08-13
    val d = date.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val m = d[0].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val h = d[1].split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val readableDate = m[2] + " " + months[Integer.parseInt(m[1]) - 1] + " " + m[0]
    val readableTime = h[0] + ":" + h[1]

    return "$readableDate    $readableTime"
}

fun getRelativeTime(unformatted: String): String {

    val date = unformatted.split("T".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    val hours = date[1].split(".000Z".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

    val formatted = date[0] + " " + hours[0]

    val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var d: Date?
    try {
        d = df.parse(formatted)
    } catch (e: ParseException) {
        e.printStackTrace()
        d = null
    }

    var timePassed: CharSequence = ""
    if (d != null) {
        val epoch = d.time
        timePassed = DateUtils.getRelativeTimeSpanString(
            epoch,
            System.currentTimeMillis(),
            DateUtils.SECOND_IN_MILLIS
        )
    }

    return timePassed.toString()
}

fun getRelativeTime(timeInMillis: Long): String {
    return DateUtils.getRelativeTimeSpanString(
        timeInMillis * 1000,
        System.currentTimeMillis(), 0L, DateUtils.FORMAT_ABBREV_ALL
    ).toString()
}

@SuppressLint("SimpleDateFormat")
fun toReadableDate(date: String?): String? {
    val dateFormat = SimpleDateFormat("dd/MM/yyyy")
    val formatedDate = dateFormat.parse(date)
    return SimpleDateFormat("EEE, dd MMM yyy").format(formatedDate)
}

fun getReadableDateTime(date: Long): String {
    val indonesiaLocale: Locale = Locale("in", "ID")
    val cal: Calendar = Calendar.getInstance()
    val tz: TimeZone = cal.timeZone
    val sdf = SimpleDateFormat("EEEE, dd MMM yyyy  /  HH:mm", indonesiaLocale)
    sdf.timeZone = tz
    return sdf.format(Date(date * 1000))
}

fun getReadableDate(date: Long): String {
    val indonesiaLocale: Locale = Locale("in", "ID")
    val cal: Calendar = Calendar.getInstance()
    val tz: TimeZone = cal.timeZone
    val sdf = SimpleDateFormat("dd MMMM yyyy", indonesiaLocale)
    sdf.timeZone = tz
    return sdf.format(Date(date * 1000))
}

fun getMoreDays(date:Long, intervalDay:Int):Long{
    val cal = Calendar.getInstance()
    cal.time = Date(date*1000)
    cal.add(Calendar.DATE, intervalDay)
    return cal.timeInMillis
}

fun isToday(time:Long):Boolean{
    val targetDay = Calendar.getInstance()
    targetDay.timeInMillis = time

    val now = Calendar.getInstance()

    return targetDay.get(Calendar.DATE) == now.get(Calendar.DATE)
}

fun emptyString(): String {
    return ""
}

fun getReadableWeight(weight: Double): String {
    var readableWeight = "%d %s"
    when (weight) {
        in 1.0..999.9 -> {
            readableWeight = String.format(readableWeight, weight.roundToInt(), "gr")
        }
        in 1000.0..9999999.9 -> {
            var result = weight % 1000
            if (result > 0) {
                readableWeight = "%.1f %s"
                readableWeight = String.format(readableWeight, weight / 1000, "kg")
            } else readableWeight = String.format(readableWeight, (weight / 1000).roundToInt(), "kg")
        }
        else -> {
            var result = weight % 1000000
            if (result > 0) {
                readableWeight = "%.1f %s"
                readableWeight = String.format(readableWeight, weight / 1000000, "ton")
            }
            readableWeight = String.format(readableWeight, (weight / 1000000).roundToInt(), "ton")
        }
    }
    return readableWeight
}

fun getReadableDimension(dimension: Double): String {
    var readableDimension = "%d %s"
    when (dimension) {
        in 1.0..99.9 -> {
            readableDimension = String.format(readableDimension, dimension.roundToInt(), "cm")
        }
        else -> {
            var result = dimension % 100
            if (result > 0) {
                readableDimension = "%.1f %s"
                readableDimension = String.format(readableDimension, dimension / 100, "m")
            } else {
                readableDimension = String.format(readableDimension, (dimension / 100).roundToInt(), "m")
            }
        }
    }
    return readableDimension
}

fun getRelativeCurrencyWithPrefix(price: Double, prefix: String? = "Rp "): String {
    val decimalFormat = DecimalFormat("0.0")
    val noDecimalFormat = DecimalFormat("0")
    return prefix + when (price) {
        in 1000.0..999999.0 -> {
            if (price % 1000.0 == 0.0) (noDecimalFormat.format(price / 1000) + "rb")
            else decimalFormat.format(price / 1000) + "rb"
        }
        in 1000000.0..999999999.0 -> {
            if (price % 1000000.0 == 0.0) (noDecimalFormat.format(price / 1000000) + "jt")
            else decimalFormat.format(price / 1000000) + "jt"
        }
        in 1000000000.0..999999999.0 -> {
            if (price % 1000000000.0 == 0.0) (noDecimalFormat.format(price / 1000000000) + "M")
            else decimalFormat.format(price / 1000000000) + "M"
        }
        else -> price.toString()
    }
}

fun getRelativeCurrency(price: Double): String {
    val decimalFormat = DecimalFormat("0.0")
    val noDecimalFormat = DecimalFormat("0")
    return when (price) {
        in 1000.0..999999.0 -> {
            if (price % 1000.0 == 0.0) (noDecimalFormat.format(price / 1000) + "rb")
            else decimalFormat.format(price / 1000) + "rb"
        }
        in 1000000.0..999999999.0 -> {
            if (price % 1000000.0 == 0.0) (noDecimalFormat.format(price / 1000000) + "jt")
            else decimalFormat.format(price / 1000000) + "jt"
        }
        in 1000000000.0..999999999.0 -> {
            if (price % 1000000000.0 == 0.0) (noDecimalFormat.format(price / 1000000000) + "M")
            else decimalFormat.format(price / 1000000000) + "M"
        }
        else -> price.toString()
    }
}

//return 1,000,000
fun Int.toCurrencyDecimalFormat(): String {
    return DecimalFormat("#,###").format(this)
}

//return Rp. 1,000,000
fun Int.toFormattedCurrencyNumber(locale: Locale = Locale("id", "ID")): String {
    val currencyCode = Currency.getInstance(locale)
    val symbol = currencyCode.getSymbol(locale)
    val currencyFormat = NumberFormat.getNumberInstance(locale)
    return "$symbol ${currencyFormat.format(this)}"
}

//return 1,000,000
fun Double.toCurrencyDecimalFormat(): String {
    return DecimalFormat("#,###").format(this)
}

//return Rp. 1.000.000
fun Double.toFormattedCurrencyNumber(locale: Locale = Locale("id", "ID")): String {
    val currencyCode = Currency.getInstance(locale)
    val symbol = currencyCode.getSymbol(locale)
    val currencyFormat = NumberFormat.getNumberInstance(locale)
    return "$symbol ${currencyFormat.format(this)}"
}

//return Rp. 10.000 - 11.000
fun getCurrencyRange(priceMin: Double, priceMax: Double): String {
    return if (priceMin == priceMax) priceMax.toFormattedCurrencyNumber()
    else {
        priceMin.toFormattedCurrencyNumber() + " - " + priceMax.toFormattedCurrencyNumber()
    }
}

fun isZipFile(fileName:String):Boolean{
    if (fileName.endsWith(".zip", true)) {
        return true
    }
    return false
}