package com.eazyclaim.android.util

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.support.annotation.NonNull
import android.support.v7.app.AlertDialog
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import com.eazyclaim.android.R
import com.nbs.nucleo.utils.extensions.onClick
import com.nbs.nucleo.utils.extensions.visible
import kotlinx.android.synthetic.main.layout_alert_dialog.view.*

fun showUncancelableDialog(
    context: Context,
    icon: Int? = null,
    title: String? = null,
    message: String? = null,
    positive: String? = null, @NonNull positiveListener: (() -> Unit)? = null,
    negative: String? = null, @NonNull negativeListener: (() -> Unit)? = null
) {
    val view: View = LayoutInflater.from(context).inflate(R.layout.layout_alert_dialog, null)

    with(view) {
        tvTitle.text = title

        if (icon != null) {
            imgIcon.visible()
            imgIcon.setImageResource(icon)
        }

        if (message != null) {
            tvMessage.visible()
            tvMessage.text = message
        }

        if (negative != null) {
            btnNegative.visible()
            btnNegative.text = negative
        }

        if (positive != null) {
            btnPositive.visible()
            btnPositive.text = positive
        }
    }

    val builder = AlertDialog.Builder(context).setView(view)
    val dialog = builder.create()


    dialog.apply {
        window?.apply {
            setLayout(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            setGravity(Gravity.CENTER)
            val inset = InsetDrawable(ColorDrawable(Color.TRANSPARENT), 60)
            setBackgroundDrawable(inset)
        }
        setCancelable(false)
        show()
    }

    with(view) {
        btnPositive.onClick {
            dialog.dismiss()
            positiveListener?.invoke()
        }

        btnNegative.onClick {
            dialog.dismiss()
            negativeListener?.invoke()
        }
    }
}

fun showCancelableDialog(
    context: Context,
    icon: Int? = null,
    title: String? = null,
    message: String? = null,
    positive: String? = null, @NonNull positiveListener: (() -> Unit)? = null,
    negative: String? = null, @NonNull negativeListener: (() -> Unit)? = null
) {
    val view: View = LayoutInflater.from(context).inflate(R.layout.layout_alert_dialog, null)

    with(view) {
        tvTitle.text = title

        if (icon != null) {
            imgIcon.visible()
            imgIcon.setImageResource(icon)
        }

        if (message != null) {
            tvMessage.visible()
            tvMessage.text = message
        }

        if (negative != null) {
            btnNegative.visible()
            btnNegative.text = negative
        }

        if (positive != null) {
            btnPositive.visible()
            btnPositive.text = positive
        }
    }

    val builder = AlertDialog.Builder(context).setView(view)
    val dialog = builder.create()


    dialog.apply {
        window?.apply {
            setLayout(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            setGravity(Gravity.CENTER)
            val inset = InsetDrawable(ColorDrawable(Color.TRANSPARENT), 60)
            setBackgroundDrawable(inset)
        }
        show()
    }

    with(view) {
        btnPositive.onClick {
            dialog.dismiss()
            positiveListener?.invoke()
        }

        btnNegative.onClick {
            dialog.dismiss()
            negativeListener?.invoke()
        }
    }
}
