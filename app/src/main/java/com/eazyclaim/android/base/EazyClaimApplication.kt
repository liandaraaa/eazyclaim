package com.eazyclaim.android.base

import android.content.Context
import android.support.multidex.MultiDex
import com.eazyclaim.android.di.apiModule
import com.eazyclaim.android.di.utilityModule
import com.nbs.nucleo.presentation.BaseApplication
import com.nbs.nucleo.utils.Timber
import org.koin.dsl.module.Module

class EazyClaimApplication : BaseApplication() {

    override fun getDefinedModules(): List<Module> {
        return listOf(
            apiModule,
            utilityModule
        )
    }

    override fun initApp() {
        Timber.plant(Timber.DebugTree())
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}