package com.nussarara.android.data.lib

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * Created by Dimas Prakoso on 02/05/2019.
 */
class ParameterInterceptor(private val params:HashMap<String, String>)
    : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val requestBuilder = original.newBuilder()
                .url(mapParameters(chain))

        val requst = requestBuilder.build()

        return chain.proceed(requst)
    }

    private fun mapParameters(chain: Interceptor.Chain): HttpUrl {
        val original = chain.request()
        val originalHttpUrl = original.url()

        val builder = originalHttpUrl.newBuilder()

        for ((key, value) in params) {
            builder.addQueryParameter(key, value)
        }

        return builder.build()
    }

}