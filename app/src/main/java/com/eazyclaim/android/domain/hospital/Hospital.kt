package com.eazyclaim.android.domain.hospital

data class Hospital(
    val name:String,
    val mrid:String,
    val address:String
)