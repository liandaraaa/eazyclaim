package com.eazyclaim.android.domain.claim

data class Claim(
    val title:String,
    val description:String,
    val status:String,
    val date:String
)