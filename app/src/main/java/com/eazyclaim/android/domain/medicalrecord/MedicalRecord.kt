package com.eazyclaim.android.domain.medicalrecord

data class MedicalRecord (
    val number:String,
    val issue:String,
    val poly: String
)