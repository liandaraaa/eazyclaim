package com.eazyclaim.android.domain.insurance

data class Insurance(
    val name:String,
    val service:String,
    val number:String
)