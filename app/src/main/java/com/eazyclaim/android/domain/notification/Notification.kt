package com.eazyclaim.android.domain.notification

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Notification(

    val title:String,
    val message:String,
    val time:String,
    val content:String,
    val isBindData:Boolean = false

) : Parcelable