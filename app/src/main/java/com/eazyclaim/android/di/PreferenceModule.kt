package com.ciputrawatermeter.android.di

import com.eazyclaim.android.util.constans.AppConstans

import com.nbs.nucleo.data.PreferenceManager
import com.nbs.nucleo.data.PreferenceManagerImpl
import org.koin.dsl.module.module

const val PREFERENCE_NAME = "preference_name"

val preferenceModule = module {

    single<PreferenceManager> { PreferenceManagerImpl(get(), get(name = PREFERENCE_NAME), get()) }

    single(name = PREFERENCE_NAME) { AppConstans.PREF_NAME }
}