package com.ciputrawatermeter.android.di

import io.reactivex.disposables.CompositeDisposable
import org.koin.dsl.module.module

val rxModule = module {

    factory { CompositeDisposable() }

}