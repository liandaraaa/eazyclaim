package com.eazyclaim.android.di

import android.support.v7.app.AppCompatActivity
import com.eazyclaim.android.data.AppDatabase
import com.eazyclaim.android.util.constans.AppConstans
import com.google.gson.Gson
import com.nbs.nucleo.data.PreferenceManager
import com.nbs.nucleo.data.PreferenceManagerImpl
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import org.greenrobot.eventbus.EventBus
import org.koin.dsl.module.module

/**
 * Created by Dimas Prakoso on 02/05/2019.
 */

val utilityModule = module {

    single<PreferenceManager> { PreferenceManagerImpl(get(), AppConstans.PREF_NAME, get()) }

    single { AppDatabase.getAppDatabase(get()) }

    factory { CompositeDisposable() }

    single { Gson() }

    single { EventBus() }

    factory { (activity: AppCompatActivity) -> RxPermissions(activity) }

}