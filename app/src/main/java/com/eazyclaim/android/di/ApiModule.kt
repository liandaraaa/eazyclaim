package com.eazyclaim.android.di

import com.eazyclaim.android.BuildConfig
import com.eazyclaim.android.data.lib.HeaderInterceptor
import com.nbs.nucleo.data.PreferenceManager
import com.nbs.nucleo.data.libs.OkHttpClientFactory
import com.nussarara.android.data.lib.ParameterInterceptor
import okhttp3.Interceptor
import org.koin.dsl.module.module

/**
 * Created by Dimas Prakoso on 02/05/2019.
 */

const val BASE_URL: String = "baseUrl"

val apiModule = module {

    single {
        return@single OkHttpClientFactory.create(interceptors = arrayOf(getHeaderInterceptor(get()),
                getParameterInterceptor()), showDebugLog = BuildConfig.DEBUG)
    }

    single(name = BASE_URL) {  }

}

private fun getParameterInterceptor(): Interceptor {
    val params = HashMap<String, String>()
    //define default parameter here

    return ParameterInterceptor(params)
}

private fun getHeaderInterceptor(preferenceManager: PreferenceManager): Interceptor {
    val headers = HashMap<String, String>()
    //define default headers here
    headers["Content-Type"] = "application/json"

    return HeaderInterceptor(headers, preferenceManager)
}