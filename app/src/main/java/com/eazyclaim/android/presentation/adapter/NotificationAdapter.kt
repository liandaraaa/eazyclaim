package com.eazyclaim.android.presentation.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.eazyclaim.android.R
import com.eazyclaim.android.domain.notification.Notification
import com.nbs.nucleo.presentation.adapter.BaseRecyclerAdapter
import com.nbs.nucleo.presentation.adapter.viewholder.BaseItemViewHolder
import com.nbs.nucleo.utils.extensions.onClick
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationAdapter(
    context: Context,
    datas:List<Notification>,
    val listener: OnNotificationClickListener? =null
):BaseRecyclerAdapter<Notification, NotificationAdapter.NotificationViewHolder>(context,datas) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        return NotificationViewHolder(mContext, getView(parent, viewType), mItemClickListener, mLongItemClickListener)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_notification
    }


    inner class NotificationViewHolder(
        context:Context,
        itemVIew:View,
        mItemClickListener: OnItemClickListener?,
        mLongItemClickListener: OnLongItemClickListener?
    ):BaseItemViewHolder<Notification>(context,itemVIew,mItemClickListener,mLongItemClickListener){
        override fun bind(data: Notification) {
            with(itemView){
                tvTitle.text = data.title
                tvTime.text = data.time
                tvMessage.text = data.message
            }

            itemView.onClick {
                listener?.onNotificaitonClicked(data)
            }
        }

    }

    interface OnNotificationClickListener{
        fun onNotificaitonClicked(data:Notification)
    }
}