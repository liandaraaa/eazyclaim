package com.eazyclaim.android.presentation.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.eazyclaim.android.R
import com.eazyclaim.android.domain.claim.Claim
import com.nbs.nucleo.presentation.adapter.BaseRecyclerAdapter
import com.nbs.nucleo.presentation.adapter.viewholder.BaseItemViewHolder
import com.nbs.nucleo.utils.extensions.onClick
import kotlinx.android.synthetic.main.item_claim_history.view.*

class ClaimHistoryAdapter(
    context: Context,
    datas:List<Claim>,
    val listener: OnClaimHistoryClickListener?=null
):BaseRecyclerAdapter<Claim, ClaimHistoryAdapter.ClaimHistoryViewHolder>(context,datas) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClaimHistoryViewHolder {
        return ClaimHistoryViewHolder(mContext, getView(parent, viewType), mItemClickListener, mLongItemClickListener)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_claim_history
    }


    inner class ClaimHistoryViewHolder(
        context:Context,
        itemVIew:View,
        mItemClickListener: OnItemClickListener?,
        mLongItemClickListener: OnLongItemClickListener?
    ):BaseItemViewHolder<Claim>(context,itemVIew,mItemClickListener,mLongItemClickListener){
        override fun bind(data: Claim) {
            with(itemView){
                tvTitle.text = data.title
                tvDescription.text = data.description
                tvSatus.text = data.status
                tvDate.text = data.date
            }

            itemView.onClick {
                listener?.onClaimClicked(data)
            }
        }

    }

    interface OnClaimHistoryClickListener{
        fun onClaimClicked(data: Claim)
    }

}