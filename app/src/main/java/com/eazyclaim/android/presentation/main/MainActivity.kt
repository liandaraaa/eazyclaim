package com.eazyclaim.android.presentation.main

import android.arch.persistence.room.Update
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetBehavior.*
import com.eazyclaim.android.R
import com.eazyclaim.android.domain.medicalrecord.MedicalRecord
import com.eazyclaim.android.presentation.claim.MakeClaimActivity
import com.eazyclaim.android.presentation.hospital.HospitalActivity
import com.eazyclaim.android.presentation.insurance.InsuranceActivity
import com.eazyclaim.android.presentation.medicalrecord.MedicalRecordActivity
import com.eazyclaim.android.presentation.notification.NotificationActivity
import com.eazyclaim.android.presentation.profile.UpdateProfileActivity
import com.eazyclaim.android.presentation.qrcode.QrCodeFragment
import com.eazyclaim.android.util.showCancelableDialog
import com.eazyclaim.android.util.showUncancelableDialog
import com.nbs.nucleo.utils.extensions.onClick
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_qrcode_dialog.*
import org.jetbrains.anko.startActivity

class MainActivity : BaseActivity() {

    companion object{
        fun start(context: Context){
            context.startActivity<MainActivity>()
        }
    }

    private val qrCodeFragment = QrCodeFragment()

    override val layoutResource: Int =  R.layout.activity_main

    override fun initAction() {

        btnUpdateProfile.onClick {
            showAlertOnUpdateProfile()
        }

        btnUpdateProfileBottom.onClick {
            showAlertOnUpdateProfile()
        }

        btnClaim.onClick {
            MakeClaimActivity.start(this)
        }

        btnQrCode.onClick {
            showQrCode()
        }

        btnNotification.onClick {
            NotificationActivity.start(this)
        }

        btnInsurance.onClick {
            InsuranceActivity.start(this)
        }

        btnHospital.onClick {
            HospitalActivity.start(this)
        }

        btnFingerprint.onClick {
            showAlertOnActivateFingerprint()
        }

        btnMedicalRecord.onClick {
            MedicalRecordActivity.start(this)
        }
    }

    override fun initIntent() {

    }

    override fun initLib() {
    }

    override fun initProcess() {
    }

    override fun initUI() {
    }

    private fun showQrCode(){
        qrCodeFragment.isCancelable = false
        qrCodeFragment.show(
            supportFragmentManager, qrCodeFragment.tag
        )
    }

    private fun showAlertOnUpdateProfile(){
        showCancelableDialog(
            context = this,
            icon = R.drawable.ic_img_dialog_profile_update,
            title = "Update Your Profile",
            message = "Complete your profile for easier health \n" +
                    "insurance claim",
            positive = "Update",
            positiveListener = {
                UpdateProfileActivity.start(this)
            },
            negative = "Not Now",
            negativeListener = {

            }
        )
    }

    private fun showAlertOnActivateFingerprint(){
        showCancelableDialog(
            context = this,
            icon = R.drawable.ic_img_dialog_fingerprint,
            title = "Activate Your Fingerprint",
            message = "Scan your fingerprint on the device scanner to continue"
        )
    }

    private fun showAlertOnFingerprintActivated(){
        showCancelableDialog(
            context = this,
            icon = R.drawable.ic_img_dialog_success,
            title = "Fingerprint Activated",
            positive = "Ok"
        )
    }

    private fun showAlertOnFingerprintAuthentication(){
        showCancelableDialog(
            context = this,
            icon = R.drawable.ic_img_dialog_fingerprint,
            title = "Fingerprint Authentication",
            message = "Touch sensor and confirm fingerprint to continue",
            positive = "Back to OTP"
        )
    }


}
