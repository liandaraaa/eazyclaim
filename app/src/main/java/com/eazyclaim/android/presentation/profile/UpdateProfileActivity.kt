package com.eazyclaim.android.presentation.profile

import android.content.Context
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.eazyclaim.android.R
import com.eazyclaim.android.util.showCancelableDialog
import com.nbs.nucleo.utils.extensions.onClick
import com.nbs.nucleo.utils.showToast
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_update_profile.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity

class UpdateProfileActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<UpdateProfileActivity>()
        }
    }

    override val layoutResource = R.layout.activity_update_profile

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
        setupToolbar(toolbar, "Update Profile", true)
    }

    override fun initAction() {
        btnSave.onClick {
            showAlertOnConfirmationUpdateProfile()
        }
    }

    override fun initProcess() {
        val genders = mutableListOf("Male", "Female")
        setUpSpinner(this, genders, spnGender)

        val provinces = mutableListOf("Jawa Barat", "Jawa Tengah", "Jawa Timur")
        setUpSpinner(this, provinces, spnProvince)

        val cities = mutableListOf("Bandung", "Bogor", "Depok")
        setUpSpinner(this, cities, spnCity)

        val districts = mutableListOf("Cibinong", "Ciputat", "Cilandak")
        setUpSpinner(this, districts, spnDistrict)

        val subDistricts = mutableListOf("Pondok Rajeg", "Cimanggis", "Tapos")
        setUpSpinner(this, districts, spnSubDistrict)
    }

    private fun setUpSpinner(context: Context, values: MutableList<String>, spinner: Spinner) {
        val spinnerAdapter = ArrayAdapter(context, R.layout.item_spinner, values)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = spinnerAdapter
    }

    private fun showAlertOnConfirmationUpdateProfile(){
        showCancelableDialog(
            context = this,
            icon = R.drawable.ic_img_dialog_confirmation_profile,
            title = "Confirmation",
            message = "Your profile will be updated. Are you sure to save your profile update?",
            positive = "Save",
            positiveListener = {
            },
            negative = "Cancel",
            negativeListener = {

            }
        )
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
