package com.eazyclaim.android.presentation.insurance


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eazyclaim.android.R
import com.eazyclaim.android.domain.claim.Claim
import com.eazyclaim.android.presentation.adapter.ClaimHistoryAdapter
import com.nbs.nucleosnucleo.presentation.BaseFragment
import kotlinx.android.synthetic.main.fragment_claim_insurance.*

/**
 * A simple [Fragment] subclass.
 */
class ClaimInsuranceFragment : BaseFragment(), ClaimHistoryAdapter.OnClaimHistoryClickListener {

    private val claimAdapter:ClaimHistoryAdapter by lazy {
        ClaimHistoryAdapter(context!!, mutableListOf(), this)
    }

    override val layoutResource: Int = R.layout.fragment_claim_insurance

    override fun initAction() {
    }

    override fun initIntent() {
    }

    override fun initLib() {
    }

    override fun initProcess() {
    }

    override fun initUI() {
        showInsurance()
    }

    private fun showInsurance(){
        val claims = mutableListOf(
            Claim(getString(R.string.sample_title_claim), getString(R.string.sample_description_claim),
                getString(R.string.sample_status_claim),getString(R.string.sample_date)),
            Claim(getString(R.string.sample_title_claim), getString(R.string.sample_description_claim),
                getString(R.string.sample_status_claim),getString(R.string.sample_date)),
            Claim(getString(R.string.sample_title_claim), getString(R.string.sample_description_claim),
                getString(R.string.sample_status_claim),getString(R.string.sample_date)),
            Claim(getString(R.string.sample_title_claim), getString(R.string.sample_description_claim),
                getString(R.string.sample_status_claim),getString(R.string.sample_date)),
            Claim(getString(R.string.sample_title_claim), getString(R.string.sample_description_claim),
                getString(R.string.sample_status_claim),getString(R.string.sample_date)),
            Claim(getString(R.string.sample_title_claim), getString(R.string.sample_description_claim),
                getString(R.string.sample_status_claim),getString(R.string.sample_date))
        )

        claimAdapter.addAll(claims)
        rvClaims.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = claimAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

    }

    override fun onClaimClicked(data: Claim) {
        ClaimantDetailActivity.start(context!!)
    }


}
