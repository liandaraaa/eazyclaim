package com.eazyclaim.android.presentation.hospital

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.eazyclaim.android.R
import com.eazyclaim.android.domain.hospital.Hospital
import com.eazyclaim.android.presentation.adapter.HospitalAdapter
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_hospital.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity

class HospitalActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<HospitalActivity>()
        }
    }

    private val hospitalAdapter:HospitalAdapter by lazy {
        HospitalAdapter(this, mutableListOf())
    }

    override val layoutResource = R.layout.activity_hospital

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
        setupToolbar(toolbar, "Hospital", true)
        showHospitals()
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

    private fun showHospitals(){
        val hospitals = mutableListOf(
            Hospital(getString(R.string.sample_hospital_name), getString(R.string.sampler_hospital_mrid), getString(R.string.sample_address_hospital)),
            Hospital(getString(R.string.sample_hospital_name), getString(R.string.sampler_hospital_mrid), getString(R.string.sample_address_hospital)),
            Hospital(getString(R.string.sample_hospital_name), getString(R.string.sampler_hospital_mrid), getString(R.string.sample_address_hospital)),
            Hospital(getString(R.string.sample_hospital_name), getString(R.string.sampler_hospital_mrid), getString(R.string.sample_address_hospital)),
            Hospital(getString(R.string.sample_hospital_name), getString(R.string.sampler_hospital_mrid), getString(R.string.sample_address_hospital)),
            Hospital(getString(R.string.sample_hospital_name), getString(R.string.sampler_hospital_mrid), getString(R.string.sample_address_hospital))
        )

        hospitalAdapter.addAll(hospitals)
        rvHospitals.apply {
            layoutManager = LinearLayoutManager(this@HospitalActivity)
            adapter = hospitalAdapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
