package com.eazyclaim.android.presentation.insurance


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eazyclaim.android.R
import com.eazyclaim.android.domain.insurance.Insurance
import com.eazyclaim.android.presentation.adapter.InsuranceAdapter
import com.nbs.nucleo.utils.extensions.onClick
import com.nbs.nucleosnucleo.presentation.BaseFragment
import kotlinx.android.synthetic.main.fragment_insurance_list.*

/**
 * A simple [Fragment] subclass.
 */
class InsuranceListFragment : BaseFragment() {

    private val insuranceAdapter:InsuranceAdapter by lazy {
        InsuranceAdapter(context!!, mutableListOf())
    }

    override val layoutResource: Int = R.layout.fragment_insurance_list

    override fun initAction() {
        btnAddInsurance.onClick {
            AddInsuranceActivity.start(context!!)
        }
    }

    override fun initIntent() {
    }

    override fun initLib() {
    }

    override fun initProcess() {
    }

    override fun initUI() {
        showInsurance()
    }

    private fun showInsurance(){
        val insurances = mutableListOf(
            Insurance(getString(R.string.sample_insurance), getString(R.string.sample_insurance_service), getString(R.string.sample_insurance_number)),
            Insurance(getString(R.string.sample_insurance), getString(R.string.sample_insurance_service), getString(R.string.sample_insurance_number)),
            Insurance(getString(R.string.sample_insurance), getString(R.string.sample_insurance_service), getString(R.string.sample_insurance_number)),
            Insurance(getString(R.string.sample_insurance), getString(R.string.sample_insurance_service), getString(R.string.sample_insurance_number)),
            Insurance(getString(R.string.sample_insurance), getString(R.string.sample_insurance_service), getString(R.string.sample_insurance_number)),
            Insurance(getString(R.string.sample_insurance), getString(R.string.sample_insurance_service), getString(R.string.sample_insurance_number))
        )

        insuranceAdapter.addAll(insurances)
        rvInsurance.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = insuranceAdapter
        }

    }


}
