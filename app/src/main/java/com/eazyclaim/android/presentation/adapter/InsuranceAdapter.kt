package com.eazyclaim.android.presentation.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.eazyclaim.android.R
import com.eazyclaim.android.domain.insurance.Insurance
import com.eazyclaim.android.domain.notification.Notification
import com.nbs.nucleo.presentation.adapter.BaseRecyclerAdapter
import com.nbs.nucleo.presentation.adapter.viewholder.BaseItemViewHolder
import com.nbs.nucleo.utils.extensions.onClick
import kotlinx.android.synthetic.main.item_insurance.view.*
import kotlinx.android.synthetic.main.item_notification.view.*

class InsuranceAdapter(
    context: Context,
    datas:List<Insurance>
):BaseRecyclerAdapter<Insurance, InsuranceAdapter.InsuranceViewHolder>(context,datas) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InsuranceViewHolder {
        return InsuranceViewHolder(mContext, getView(parent, viewType), mItemClickListener, mLongItemClickListener)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_insurance
    }


    inner class InsuranceViewHolder(
        context:Context,
        itemVIew:View,
        mItemClickListener: OnItemClickListener?,
        mLongItemClickListener: OnLongItemClickListener?
    ):BaseItemViewHolder<Insurance>(context,itemVIew,mItemClickListener,mLongItemClickListener){
        override fun bind(data: Insurance) {
            with(itemView){
                tvName.text = data.name
                tvService.text = data.service
                tvNumber.text = data.number
            }
        }

    }

}