package com.eazyclaim.android.presentation.reusableviews

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class SimplePagerAdapter<Data : Any>(
    private val mContext: Context,
    private val mDatas: MutableList<Data> = mutableListOf()
) : PagerAdapter() {

    override fun destroyItem(container: ViewGroup, position: Int, item: Any) {
        container.removeView(item as View)
    }

    override fun getCount(): Int {
        return mDatas.size
    }

    override fun isViewFromObject(view: View, item: Any): Boolean {
        return view === item
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(mContext)
        val view = inflater.inflate(getLayoutRes(), container, false) as ViewGroup

        bind(view, mDatas[position])

        container.addView(view)
        return view
    }

    fun getDatas(): List<Data> {
        return mDatas
    }

    fun add(item: Data) {
        mDatas.add(item)
        notifyDataSetChanged()
    }

    fun addAll(items: List<Data>) {
        add(items)
    }

    fun add(item: Data, position: Int) {
        mDatas.add(position, item)
        notifyDataSetChanged()
    }

    fun add(items: List<Data>) {
        val size = items.size
        for (i in 0 until size) {
            mDatas.add(items[i])
        }
        notifyDataSetChanged()
    }

    fun addOrUpdate(item: Data) {
        val i = mDatas.indexOf(item)
        if (i >= 0) {
            mDatas[i] = item
            notifyDataSetChanged()
        } else {
            add(item)
        }
    }

    fun addOrUpdate(items: List<Data>) {
        val size = items.size
        for (i in 0 until size) {
            val item = items[i]
            val x = mDatas.indexOf(item)
            if (x >= 0) {
                mDatas[x] = item
            } else {
                add(item)
            }
        }
        notifyDataSetChanged()
    }

    fun addOrUpdateToFirst(items: List<Data>) {
        val size = items.size
        for (i in 0 until size) {
            val item = items[i]
            val x = mDatas.indexOf(item)
            if (x >= 0) {
                mDatas[x] = item
            } else {
                add(item, 0)
            }
        }
        notifyDataSetChanged()
    }

    fun addToFirst(item: Data) {
        mDatas.add(0, item)
        notifyDataSetChanged()
    }

    fun addToFirst(items: List<Data>) {
        mDatas.addAll(0, items)
        notifyDataSetChanged()
    }

    fun remove(position: Int) {
        if (position >= 0 && position < mDatas.size) {
            mDatas.removeAt(position)
            notifyDataSetChanged()
        }
    }

    fun remove(item: Data) {
        val position = mDatas.indexOf(item)
        remove(position)
    }

    fun clear() {
        mDatas.clear()
        notifyDataSetChanged()
    }

    abstract fun getLayoutRes(): Int
    abstract fun bind(view: View, data: Data)
}