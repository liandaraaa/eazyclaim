package com.eazyclaim.android.presentation.notification


import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eazyclaim.android.R
import com.eazyclaim.android.domain.notification.Notification
import com.eazyclaim.android.presentation.hospital.HospitalActivity
import com.eazyclaim.android.util.constans.BundleKeys
import com.eazyclaim.android.util.showUncancelableDialog
import com.nbs.nucleo.utils.extensions.onClick
import kotlinx.android.synthetic.main.layout_notification_dialog.*

/**
 * A simple [Fragment] subclass.
 */
class NotificationDetailFragment : BottomSheetDialogFragment(){

    companion object{
        fun newInstance(notification:Notification):NotificationDetailFragment{
            val fragment = NotificationDetailFragment()
            val bundle = Bundle()
            bundle.putParcelable(BundleKeys.KEY_NOTIFICATION, notification)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.layout_notification_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)

        val notification = arguments?.getParcelable(BundleKeys.KEY_NOTIFICATION) as Notification

        btnNotificationAction.text = if (notification.isBindData) "Bind Data" else "Close"
        btnNotificationAction.onClick {
            if (notification.isBindData){
                showAlertConfirmationBindData()
            }else{
                dismiss()
            }
        }
    }

    private fun showAlertConfirmationBindData(){
        showUncancelableDialog(
            context = context!!,
            title = "Bind Your Data",
            icon = R.drawable.ic_img_dialog_confirmation,
            message = "You’re about to bind data with Rumah Sakit Haji Jakarta data. Are you sure to bind?",
            positive = "Bind Data",
            positiveListener = {
                HospitalActivity.start(context!!)
            },
            negative = "close",
            negativeListener = {}
        )
    }

}
