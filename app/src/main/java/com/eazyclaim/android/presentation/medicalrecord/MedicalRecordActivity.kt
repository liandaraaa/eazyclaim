package com.eazyclaim.android.presentation.medicalrecord

import android.content.Context
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.eazyclaim.android.R
import com.eazyclaim.android.domain.medicalrecord.MedicalRecord
import com.eazyclaim.android.presentation.adapter.MedicalRecordAdapter
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_medical_record.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity

class MedicalRecordActivity : BaseActivity(), MedicalRecordAdapter.OnMedicalRecordItemClickListener {

    companion object {
        fun start(context: Context) {
            context.startActivity<MedicalRecordActivity>()
        }
    }

    private val medicalRecordAdapter:MedicalRecordAdapter by lazy {
        MedicalRecordAdapter(this, mutableListOf(), this)
    }

    override val layoutResource = R.layout.activity_medical_record

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
        setupToolbar(toolbar, "Medical Record", true)
        showMedicalRecords()
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

    private fun showMedicalRecords(){
        val medicalRecords = mutableListOf(
            MedicalRecord(getString(R.string.sample_mr_number), getString(R.string.sample_issue), getString(R.string.sample_poly)),
            MedicalRecord(getString(R.string.sample_mr_number), getString(R.string.sample_issue), getString(R.string.sample_poly)),
            MedicalRecord(getString(R.string.sample_mr_number), getString(R.string.sample_issue), getString(R.string.sample_poly)),
            MedicalRecord(getString(R.string.sample_mr_number), getString(R.string.sample_issue), getString(R.string.sample_poly)),
            MedicalRecord(getString(R.string.sample_mr_number), getString(R.string.sample_issue), getString(R.string.sample_poly)),
            MedicalRecord(getString(R.string.sample_mr_number), getString(R.string.sample_issue), getString(R.string.sample_poly)),
            MedicalRecord(getString(R.string.sample_mr_number), getString(R.string.sample_issue), getString(R.string.sample_poly))
        )

        medicalRecordAdapter.addAll(medicalRecords)
        rvMedicalRecords.apply {
            layoutManager = LinearLayoutManager(this@MedicalRecordActivity)
            adapter = medicalRecordAdapter
            addItemDecoration(DividerItemDecoration(this@MedicalRecordActivity, DividerItemDecoration.VERTICAL))
        }
    }

    override fun onMedicalRecordClicked(data: MedicalRecord) {
        MedicalRecordDetailActivity.start(this)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
