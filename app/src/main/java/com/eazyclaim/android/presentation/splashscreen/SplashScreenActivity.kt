package com.eazyclaim.android.presentation.splashscreen

import android.content.Context
import com.eazyclaim.android.R
import com.eazyclaim.android.presentation.login.LoginActivity
import com.eazyclaim.android.presentation.main.MainActivity
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.startActivity

class SplashScreenActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<SplashScreenActivity>()
        }
    }

    override val layoutResource = R.layout.activity_splash_screen

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {

        GlobalScope.launch {
            delay(3000)
            LoginActivity.start(this@SplashScreenActivity)
            finish()
        }
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

}
