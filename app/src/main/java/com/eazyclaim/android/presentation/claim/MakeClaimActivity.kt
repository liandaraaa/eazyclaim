package com.eazyclaim.android.presentation.claim

import android.content.Context
import android.view.MenuItem
import com.afollestad.materialdialogs.util.DialogUtils
import com.eazyclaim.android.R
import com.eazyclaim.android.util.extensions.replaceFragment
import com.eazyclaim.android.util.showUncancelableDialog
import com.nbs.nucleo.utils.extensions.onClick
import com.nbs.nucleo.utils.showToast
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_make_claim.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity

class MakeClaimActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<MakeClaimActivity>()
        }
    }

    private var currentStep = 0

    override val layoutResource = R.layout.activity_make_claim

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
        setupToolbar(toolbar, "Make Claim", true)
        stepClaim.apply {
            setSteps(
                mutableListOf(
                    "Insurance",
                    "Medical Record",
                    "Summary"
                )

            )
        }

        setupStep()
    }

    override fun initAction() {
        btnNext.onClick {
            if (currentStep < stepClaim.stepCount - 1) {
                currentStep++
                stepClaim.go(currentStep, true)
                setupStep()
                btnNext.text = if(currentStep == stepClaim.stepCount - 1) "Make Claim" else "Next"
            }
            else {
                showAlertConfirmationOnClaim()
                stepClaim.done(true)
            }
        }
    }

    override fun initProcess() {
    }

    private fun setupStep(){
        when(currentStep){
            0 ->{
                replaceFragment(
                    R.id.flClaim,
                    SelectInsuranceFragment(),
                    false
                )
            }
            1 ->{
                replaceFragment(
                    R.id.flClaim,
                    MedicalRecordFormFragment(),
                    true
                )
            }
            2 ->{
                replaceFragment(
                    R.id.flClaim,
                    ClaimSummaryFragment(),
                    true
                )
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showAlertConfirmationOnClaim(){
        showUncancelableDialog(
            context = this,
            icon = R.drawable.ic_img_dialog_confirmation,
            title = "Confirmation",
            message = "You’ll make a claim with this data. Are you sure the inputed data is correct?",
            positive = "Make Claim",
            positiveListener = {
                showToast("Claimmed")
            },
            negative = "Cancel",
            negativeListener = {}
        )
    }
}
