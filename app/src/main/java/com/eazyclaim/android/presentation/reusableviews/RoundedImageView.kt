package com.eazyclaim.android.presentation.reuseableviews

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.TypedValue
import com.eazyclaim.android.R

/**
 * Created by Dimas Prakoso on 12/04/2019.
 */
class RoundedImageView : AppCompatImageView {

    private var cornerRadius = 0f

    private var imgRadius: Float = 0.toFloat()

    private var isOnlyTop = false

    private var isOnlyBottom = false

    private lateinit var paint: Paint

    private lateinit var maskPaint: Paint

    private lateinit var metrics: DisplayMetrics

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    fun init(attrs: AttributeSet?) {
        metrics = context.resources.displayMetrics
        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        maskPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.FILTER_BITMAP_FLAG)
        maskPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        setWillNotDraw(false)
        parseAttrs(attrs)
        imgRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, cornerRadius, metrics);
    }

    private fun parseAttrs(attrs: AttributeSet?) {
        if (attrs != null) {
            val typedArray = context
                .obtainStyledAttributes(attrs, R.styleable.RoundedImageView)
            try {
                cornerRadius = typedArray.getFloat(R.styleable.RoundedImageView_img_radius, 0f)
                isOnlyTop = typedArray.getBoolean(R.styleable.RoundedImageView_is_only_top, false)
            } finally {
                typedArray.recycle()
            }
        }
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas?) {
        val offScreenBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val offScreenCanvas = Canvas(offScreenBitmap)
        super.onDraw(offScreenCanvas)
        val maskBitmap = createMask(width, height)
        offScreenCanvas.drawBitmap(maskBitmap, 0f, 0f, maskPaint)
        canvas?.drawBitmap(offScreenBitmap, 0f, 0f, paint)


    }

    private fun createMask(width: Int, height: Int): Bitmap {
        val mask = Bitmap.createBitmap(width, height, Bitmap.Config.ALPHA_8)
        val canvas = Canvas(mask)

        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = Color.WHITE

        if (isOnlyTop){
            canvas.drawRect(0f, imgRadius, width.toFloat(), height.toFloat(), paint)
        }
        else if (isOnlyBottom){
            canvas.drawRect(0f, 0f, imgRadius, height.toFloat(), paint)
        }
        else{
            canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
        }
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        canvas.drawRoundRect(RectF(0f, 0f, width.toFloat(), height.toFloat()), imgRadius, imgRadius, paint)

        return mask
    }
}
