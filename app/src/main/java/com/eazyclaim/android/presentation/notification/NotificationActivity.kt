package com.eazyclaim.android.presentation.notification

import android.content.Context
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetBehavior.STATE_EXPANDED
import android.support.design.widget.BottomSheetBehavior.STATE_HIDDEN
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.eazyclaim.android.R
import com.eazyclaim.android.domain.notification.Notification
import com.eazyclaim.android.presentation.adapter.NotificationAdapter
import com.eazyclaim.android.util.showUncancelableDialog
import com.nbs.nucleo.utils.extensions.onClick
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.layout_notification_dialog.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity

class NotificationActivity : BaseActivity(), NotificationAdapter.OnNotificationClickListener {

    companion object {
        fun start(context: Context) {
            context.startActivity<NotificationActivity>()
        }
    }

    private val notificationAdapter:NotificationAdapter by lazy {
        NotificationAdapter(this, mutableListOf(), this)
    }

    override val layoutResource = R.layout.activity_notification

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
        setupToolbar(toolbar, "Notification", true)

        showNotification()
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

    private fun showNotification(){
        val notifications = mutableListOf<Notification>(
            Notification(getString(R.string.sample_title_notification),
                getString(R.string.sample_message_notification),
                getString(R.string.sample_time_notification),
                getString(R.string.sample_content_notification)),
            Notification(getString(R.string.sample_title_bind_notification),
                getString(R.string.sample_message_bind_notification),
                getString(R.string.sample_time_notification),
                getString(R.string.sample_content_bind_notification),
                true),
            Notification(getString(R.string.sample_title_notification),
                getString(R.string.sample_message_notification),
                getString(R.string.sample_time_notification),
                getString(R.string.sample_content_notification)),
            Notification(getString(R.string.sample_title_notification),
                getString(R.string.sample_message_notification),
                getString(R.string.sample_time_notification),
                getString(R.string.sample_content_notification),
                true),
            Notification(getString(R.string.sample_title_notification),
                getString(R.string.sample_message_notification),
                getString(R.string.sample_time_notification),
                getString(R.string.sample_content_notification))
        )

        notificationAdapter.addAll(notifications)

        rvNotifications.apply {
            layoutManager = LinearLayoutManager(this@NotificationActivity)
            adapter = notificationAdapter
        }

    }

    override fun onNotificaitonClicked(data: Notification) {
        val notificationDialog = NotificationDetailFragment.newInstance(data)
        notificationDialog.isCancelable = false
        notificationDialog.show(
            supportFragmentManager, notificationDialog.tag
        )
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
