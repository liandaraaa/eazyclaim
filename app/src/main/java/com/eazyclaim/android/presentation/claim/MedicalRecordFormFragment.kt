package com.eazyclaim.android.presentation.claim


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.eazyclaim.android.R
import com.nbs.nucleosnucleo.presentation.BaseFragment

/**
 * A simple [Fragment] subclass.
 */
class MedicalRecordFormFragment : BaseFragment() {

    companion object{
        fun newInstance():MedicalRecordFormFragment{
            var fragment = MedicalRecordFormFragment()
            var bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    override val layoutResource: Int = R.layout.fragment_medical_record_form

    override fun initAction() {
    }

    override fun initIntent() {
    }

    override fun initLib() {
    }

    override fun initProcess() {
    }

    override fun initUI() {
    }


}
