package com.eazyclaim.android.presentation.login

import android.content.Context
import com.eazyclaim.android.R
import com.eazyclaim.android.presentation.main.MainActivity
import com.eazyclaim.android.presentation.register.RegisterActivity
import com.nbs.nucleosnucleo.presentation.BaseActivity
import com.nbs.validacion.util.onClick
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity

class LoginActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<LoginActivity>()
        }
    }

    override val layoutResource = R.layout.activity_login

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
    }

    override fun initAction() {
        btnLogin.onClick {
            MainActivity.start(this)
        }

        btnRegister.onClick {
            RegisterActivity.start(this)
        }
    }

    override fun initProcess() {
    }

}
