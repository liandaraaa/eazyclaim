package com.eazyclaim.android.presentation.adapter

import android.support.v4.app.FragmentManager
import com.nbs.nucleo.presentation.adapter.BasePagerAdapter
import com.nbs.nucleosnucleo.presentation.BaseFragment

class GeneralPagerAdapter(fm: FragmentManager, fragments: List<BaseFragment>, titles: List<String> = mutableListOf()) :
    BasePagerAdapter<BaseFragment>(fm, fragments, titles) {

    override fun getItem(position: Int): BaseFragment = fragments[position]
}