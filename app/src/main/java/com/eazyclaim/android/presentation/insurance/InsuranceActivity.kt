package com.eazyclaim.android.presentation.insurance

import android.content.Context
import android.view.MenuItem
import com.eazyclaim.android.R
import com.eazyclaim.android.presentation.adapter.GeneralPagerAdapter
import com.nbs.nucleosnucleo.presentation.BaseActivity
import com.nbs.nucleosnucleo.presentation.BaseFragment
import kotlinx.android.synthetic.main.activity_insurance.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity

class InsuranceActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<InsuranceActivity>()
        }
    }

    override val layoutResource = R.layout.activity_insurance

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
        setupToolbar(toolbar, "Insurance", true)
        setupViewPager()
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

    private fun getTitleTab(): MutableList<String> {
        val title = mutableListOf<String>()
        title.add("Insurance".toUpperCase())
        title.add("Claim History".toUpperCase())
        return title
    }

    private fun getMainFragment(): MutableList<BaseFragment> {
        return mutableListOf(
            InsuranceListFragment(),
            ClaimInsuranceFragment()
        )
    }

    private fun setupViewPager() {
        val pagerAdapter = GeneralPagerAdapter(
            fm = supportFragmentManager,
            fragments = getMainFragment(),
            titles = getTitleTab()
        )

        vpInsurance.apply {
            adapter = pagerAdapter
            offscreenPageLimit = 2
        }

        tabInsurance.setupWithViewPager(vpInsurance)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
