package com.eazyclaim.android.presentation.medicalrecord

import android.content.Context
import android.view.MenuItem
import com.eazyclaim.android.R
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity

class MedicalRecordDetailActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<MedicalRecordDetailActivity>()
        }
    }

    override val layoutResource = R.layout.activity_medical_record_detail

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
        setupToolbar(toolbar, "Medical Record Detail", true)
    }

    override fun initAction() {
    }

    override fun initProcess() {
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
