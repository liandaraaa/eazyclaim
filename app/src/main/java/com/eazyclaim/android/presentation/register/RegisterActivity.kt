package com.eazyclaim.android.presentation.register

import android.content.Context
import com.eazyclaim.android.R
import com.eazyclaim.android.presentation.otp.OtpActivity
import com.nbs.nucleo.utils.extensions.onClick
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.startActivity

class RegisterActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<RegisterActivity>()
        }
    }

    override val layoutResource = R.layout.activity_register

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
    }

    override fun initAction() {
        btnRegister.onClick {
            OtpActivity.start(this)
        }
    }

    override fun initProcess() {
    }

}
