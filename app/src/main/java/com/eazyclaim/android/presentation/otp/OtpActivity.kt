package com.eazyclaim.android.presentation.otp

import android.content.Context
import com.eazyclaim.android.R
import com.eazyclaim.android.presentation.main.MainActivity
import com.nbs.nucleo.utils.extensions.onTextChanged
import com.nbs.nucleosnucleo.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_otp.*
import org.jetbrains.anko.startActivity

class OtpActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity<OtpActivity>()
        }
    }

    override val layoutResource = R.layout.activity_otp

    override fun initLib() {
    }

    override fun initIntent() {
    }

    override fun initUI() {
    }

    override fun initAction() {
        edtOtp.onTextChanged {
            if (it.length == 4){
                MainActivity.start(this)
            }
        }
    }

    override fun initProcess() {
    }

}
